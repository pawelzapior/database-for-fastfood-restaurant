-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 02 Sie 2021, 20:43
-- Wersja serwera: 10.4.18-MariaDB
-- Wersja PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `fastfood`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders`
--

CREATE TABLE `orders` (
  `id_order` int(10) UNSIGNED NOT NULL,
  `amount_to_pay` float UNSIGNED NOT NULL DEFAULT 0,
  `payment_method` enum('cash','card') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `orders`
--

INSERT INTO `orders` (`id_order`, `amount_to_pay`, `payment_method`) VALUES
(1, 46, 'card');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `order_items`
--

CREATE TABLE `order_items` (
  `id_order_items` int(10) UNSIGNED NOT NULL,
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_product` int(10) UNSIGNED DEFAULT NULL,
  `id_set` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `order_items`
--

INSERT INTO `order_items` (`id_order_items`, `id_order`, `id_product`, `id_set`) VALUES
(1, 1, 201, NULL),
(3, 1, NULL, 1002);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `products`
--

CREATE TABLE `products` (
  `id_product` int(10) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `price` float NOT NULL,
  `drink_temperature` enum('hot','cold') DEFAULT NULL,
  `drink_size` enum('small','medium','large') DEFAULT NULL,
  `sandwich_type` enum('hamburger','wrap') DEFAULT NULL,
  `sandwich_meat` enum('chicken','beef','vege') DEFAULT NULL,
  `sandwich_vege` enum('yes','no') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `products`
--

INSERT INTO `products` (`id_product`, `name`, `price`, `drink_temperature`, `drink_size`, `sandwich_type`, `sandwich_meat`, `sandwich_vege`) VALUES
(101, 'water', 5, 'cold', 'small', NULL, NULL, NULL),
(102, 'water', 7, 'cold', 'medium', NULL, NULL, NULL),
(103, 'water', 8, 'cold', 'large', NULL, NULL, NULL),
(104, 'soda', 8, 'cold', 'small', NULL, NULL, NULL),
(105, 'soda', 9.5, 'cold', 'medium', NULL, NULL, NULL),
(106, 'soda', 10, 'cold', 'large', NULL, NULL, NULL),
(107, 'tea', 6, 'hot', 'small', NULL, NULL, NULL),
(108, 'tea', 7.5, 'hot', 'medium', NULL, NULL, NULL),
(109, 'tea', 8, 'hot', 'large', NULL, NULL, NULL),
(110, 'coffe', 7, 'hot', 'small', NULL, NULL, NULL),
(111, 'coffe', 8, 'hot', 'medium', NULL, NULL, NULL),
(112, 'coffe', 8.5, 'hot', 'large', NULL, NULL, NULL),
(201, 'classic hamburger', 16, NULL, NULL, 'hamburger', 'beef', 'no'),
(202, 'cheeseburger', 18, NULL, NULL, 'hamburger', 'beef', 'no'),
(203, 'spicyburger', 18, NULL, NULL, 'hamburger', 'beef', 'no'),
(204, 'chickenburger', 17, NULL, NULL, 'hamburger', 'chicken', 'no'),
(206, 'tofuburger', 18, NULL, NULL, 'hamburger', 'vege', 'yes'),
(207, 'fajita wrap', 19, NULL, NULL, 'wrap', 'chicken', 'no'),
(208, 'falafel wrap', 17, NULL, NULL, 'wrap', 'vege', 'yes');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `sets`
--

CREATE TABLE `sets` (
  `id_set` int(10) UNSIGNED NOT NULL,
  `id_product1` int(10) UNSIGNED NOT NULL,
  `id_product2` int(10) UNSIGNED NOT NULL,
  `id_product3` int(10) UNSIGNED NOT NULL,
  `price` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `sets`
--

INSERT INTO `sets` (`id_set`, `id_product1`, `id_product2`, `id_product3`, `price`) VALUES
(1001, 202, 201, 105, 25),
(1002, 206, 208, 106, 30);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_order`);

--
-- Indeksy dla tabeli `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id_order_items`);

--
-- Indeksy dla tabeli `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_product`);

--
-- Indeksy dla tabeli `sets`
--
ALTER TABLE `sets`
  ADD PRIMARY KEY (`id_set`);

--
-- AUTO_INCREMENT dla zrzuconych tabel
--

--
-- AUTO_INCREMENT dla tabeli `orders`
--
ALTER TABLE `orders`
  MODIFY `id_order` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id_order_items` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
